<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//header("Content-Type: application/json");
//header("Access-Control-Allow-Origin: *");

class Admin extends CI_Controller {

    private $path;
    public function __construct()
   {
       parent::__construct();
       date_default_timezone_set("Asia/Bangkok"); 
       $this->path = dirname(__FILE__,3);
   }
   private function parse_html($html,$replace,$replacer)
   {
       $doc = file_get_contents($this->path."/theme/".$html.".html");
       $result = preg_replace($replace,$replacer,$doc);
       return $result;
   }
   private function include_html($html)
   {
       $doc = file_get_contents($this->path."/theme/".$html.".html"); 
       return $doc;
   }
   public function login()
   {
       $rep = ["/{judul}/","/{caption}/","/{url}/"];
       $repl = ["Alt Shue Admin","Alt Shue Sepatu Modern",base_url()];
       $out = $this->parse_html("login",$rep,$repl);
       echo $out;
   }
   public function home()
   {
        $token = $this->input->get("token");
        $navbar = $this->include_html("navbar");
        $rep = ["/{url}/","/{judul}/","/{token}/","/{navbar}/"];
        $repl = [base_url(),"Alt Shue Admin",$token,""];
        $navs = preg_replace($rep,$repl,$navbar);
        $repl = [base_url(),"Alt Shue Admin",$token,$navs];
        $out = $this->parse_html("header",$rep,$repl);
        $out .= $this->parse_html("home",$rep,$repl);
        $out .= $this->parse_html("footer",$rep,$repl);
        echo $out; 
   }
   public function articles()
   {
        $token = $this->input->get("token");
        $navbar = $this->include_html("navbar");
        $rep = ["/{url}/","/{judul}/","/{token}/","/{navbar}/"];
        $repl = [base_url(),"Alt Shue Admin",$token,""];
        $navs = preg_replace($rep,$repl,$navbar);
        $repl = [base_url(),"Alt Shue Admin",$token,$navs];
        $out = $this->parse_html("header",$rep,$repl);
        $out .= $this->parse_html("articles",$rep,$repl);
        $out .= $this->parse_html("footer",$rep,$repl);
        echo $out;
   }
    public function about()
   {
        $token = $this->input->get("token");
        $navbar = $this->include_html("navbar");
        $rep = ["/{url}/","/{judul}/","/{token}/","/{navbar}/"];
        $repl = [base_url(),"Alt Shue Admin",$token,""];
        $navs = preg_replace($rep,$repl,$navbar);
        $repl = [base_url(),"Alt Shue Admin",$token,$navs];
        $out = $this->parse_html("header",$rep,$repl); 
        $out .= $this->parse_html("about",$rep,$repl);
        $out .= $this->parse_html("footer",$rep,$repl);
        echo $out;
   }
   public function gift()
   {
        $token = $this->input->get("token");
        $navbar = $this->include_html("navbar");
        $rep = ["/{url}/","/{judul}/","/{token}/","/{navbar}/"];
        $repl = [base_url(),"Alt Shue Admin",$token,""];
        $navs = preg_replace($rep,$repl,$navbar);
        $repl = [base_url(),"Alt Shue Admin",$token,$navs];
        $out = $this->parse_html("header",$rep,$repl); 
        $out .= $this->parse_html("gift",$rep,$repl);
        $out .= $this->parse_html("footer",$rep,$repl);
        echo $out;
   }
   public function gift_redeem()
   {
        $token = $this->input->get("token");
        $navbar = $this->include_html("navbar");
        $rep = ["/{url}/","/{judul}/","/{token}/","/{navbar}/"];
        $repl = [base_url(),"Alt Shue Admin",$token,""];
        $navs = preg_replace($rep,$repl,$navbar);
        $repl = [base_url(),"Alt Shue Admin",$token,$navs];
        $out = $this->parse_html("header",$rep,$repl); 
        $out .= $this->parse_html("gift_redeem",$rep,$repl);
        $out .= $this->parse_html("footer",$rep,$repl);
        echo $out;
   }
   public function socmed()
   {
        $token = $this->input->get("token");
        $navbar = $this->include_html("navbar");
        $rep = ["/{url}/","/{judul}/","/{token}/","/{navbar}/"];
        $repl = [base_url(),"Alt Shue Admin",$token,""];
        $navs = preg_replace($rep,$repl,$navbar);
        $repl = [base_url(),"Alt Shue Admin",$token,$navs];
        $out = $this->parse_html("header",$rep,$repl); 
        $out .= $this->parse_html("socmed",$rep,$repl);
        $out .= $this->parse_html("footer",$rep,$repl);
        echo $out;
   }
}