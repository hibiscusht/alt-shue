<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");

class Api extends CI_Controller {

    private $path;
    public function __construct()
   {
       parent::__construct();
       date_default_timezone_set("Asia/Bangkok");
       $this->load->model("apimodel");
       $this->path = dirname(__FILE__,3);
   }

   private function GetHeader($name)

   {

       $header = getallheaders();

       return $header[$name];
   }

   private function getMember($token)
   {
       $where = ["Token"=>$token];
       $res = $this->apimodel->get("members")->where($where);
       return $res;
   }

   private function doUpload($file)
   {
     $files = $_FILES[$file];
     $type = "";
     switch($files["type"]){
         case "image/jpeg": $type = ".jpeg"; break;
         case "image/png": $type = ".png"; break;
     }
     $name = "img".date("YmdHis").$type;
     move_uploaded_file($files["tmp_name"],$this->path."/uploads/".$name);
     return $name;
   }
   private function clearImage($id,$column,$table = "members")
   {
       $res = $this->apimodel->query("SELECT $column FROM $table WHERE Id = $id");
       if(!is_null($res[0]->$column) && file_exists($this->path."/uploads/".$res[0]->$column)){
           unlink($this->path."/uploads/".$res[0]->$column);
       }

   }

    private function Output($code,$msg,$data)
    {
        header("HTTP/1.0 $code");
        $result = [
            "status"=>$code,
            "message"=>$msg,
            "data"=>$data
        ];
        echo json_encode($result);
    }

    private function Emailer($to,$body,$subject = "")

    {

        $this->load->library('email');

        $email['protocol'] = 'smtp';

 

            $email['smtp_host'] = 's1.ct8.pl';
            $email['smtp_user'] = 'free-shue@advoqu.ct8.pl';
            $email['smtp_pass'] = '1g*C_pTy4lrCapB3Ro^bH80z7KP85/';

    

            $email['smtp_crypto'] = 'tls'; 

    

            $email['smtp_timeout'] = 60;

    

            $email['smtp_port'] = 587;


            $email['smtp_keepalive'] = true;

    

            $email['mailtype'] = 'html';    

    

    

        $this->email->initialize($email);

        $this->email->from('free-shue@advoqu.ct8.pl', 'Alt Shue App');

        $this->email->to($to);

        $subj = $subject == "" ? "Token For New Client Registration" : $subject;

        $this->email->subject($subj); $this->email->message($body);

        $this->email->send(); 

       return $this->email->print_debugger();

    }

    public function member_reset_password()
    {
        $subject = "You have requested for Password reset"; 
        $email = $this->input->post("email");
        $reset = rand(1000,9999);
        $message = "Your reset key: $reset";
        $res = $this->apimodel->get("members")->where(["Email"=>$email]);
        if(count($res) > 0){
            $this->apimodel->get("members")->row($res[0]->Id)->update(["Reset"=>$reset]);
            $res = $this->emailer($email,$message,$subject);
            $this->Output(200,"success",["UserEmail"=>$email]);
        } else {
            $this->Output(401,"invalid credential",[]);
        }
      
    }

    public function member_confirm_otp()
    {
        $email = $this->input->post("email");
        $pin = $this->input->post("otp"); 
        $res = $this->apimodel->get("members")->where(["Email"=>$email,"Reset"=>$pin]);
        if($pin == ""){
            $this->Output(400,"whoops, no otp sent",[]);
        } else {
            if(count($res) > 0){
                $this->Output(200,"success",["MemberId"=>$res[0]->Id]);
            } else {
                $this->Output(403,"whoops, member unknown",[]);
            }
        }
       
    }

    public function member_confirm_password()
    {
        $id = $this->input->post("MemberId");
        $pass = $this->input->post("password");
        $res = $this->apimodel->get("members")->where(["Id"=>$id]);
        if(count($res) > 0){
            $this->apimodel->get("members")->row($res[0]->Id)->update(["Password"=>md5($pass)]);
            $this->Output(200,"success",["Token"=>$res[0]->Token]);
        } else {
            $this->Output(403,"whoops, member unknown",[]);
        } 
    }

    public function member_add()
    {
        $token = md5(rand(1,1000000));
        $post = $this->input->post();
        foreach($post as $key => $row){
            $post["Password"] = md5($row);
        }
        $post["Token"] = $token;
        $post["RefCode"] = $token;
        $chk = $this->apimodel->get("members")->where(["Email"=>$post["Email"]]);
        if(count($chk) > 0){
            $this->Output(403,"Whoops, user sudah ada",[]);
        } else {
             $res = $this->apimodel->get("members")->create($post);
            $res2 = $this->apimodel->get("member_profile")->create(["MemberId"=>$res,"MemberLevel"=>"Beginner","MemberExp"=>0,"MemberAP"=>0]);
            $data["Token"] = $token;  
            $this->Output(200,"Yay,pendaftaran sukses",$data);
        }
       
    }
    public function member_ektp()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
       // $this->member_connect($token);
        $post = $this->input->post();
        if(isset($_FILES["Ektp"])){
            $this->clearImage($member[0]->Id,"Ektp");
            $post["Ektp"] = $this->doUpload("Ektp");
        }
        $this->apimodel->get("members")->row($member[0]->Id)->update($post);
        $this->Output(200,"Yay,verifikasi Ektp sukses",[]);
    
    }
    public function member_data_update()
    {
        $token = $this->getHeader("x-token");
       // $this->member_connect($token);
        $member = $this->getMember($token);
        $post = $this->input->post();
        if(isset($_FILES["Foto"])){
            $this->clearImage($member[0]->Id,"Photo");
            $post["Photo"] = $this->doUpload("Foto");
        }
        $this->apimodel->get("members")->row($member[0]->Id)->update($post);
        $this->Output(200,"sukses",[]);
    }
    public function member_password()
    {
        $token = $this->getHeader("x-token");
       // $this->member_connect($token);
        $member = $this->getMember($token);
        $post = $this->input->post();
        foreach($post as $row){
            $post["Password"] = md5($row);
        }
        $this->apimodel->get("members")->row($member[0]->Id)->update(["Password"=>$post["Password"]]);
        $this->Output(200,"sukses",[]);
    }
    public function member_login()
    {
        $credential = $this->input->post("email");
        $password = md5($this->input->post("password"));
        $res = $this->apimodel->query("SELECT Token,EktpId FROM members WHERE (Email = '$credential' OR Phone = '$credential') AND Password = '$password'");
        if(count($res) > 0){

            $post = [
                "Steps"=>0,
                "Hours"=>0,
                "Distance"=>0,
                "LoginCount"=>1,
                "ConnectHours"=>0
            ];
            $member = $this->getMember($res[0]->Token);
            $post["MemberId"] = $member[0]->Id;
            $this->apimodel->get("member_activity")->create($post);

            $ektp = is_null($res[0]->EktpId) ? false : true;
            $this->Output(200,"sukses",["Token"=>$res[0]->Token,"EktpVerified"=>$ektp]);
        } else {
            $this->Output(412,"Whoops, akun tidak dikenal",[]);
        }
    }

    public function member_activity_add()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $post = $this->input->post();
        $post["MemberId"] = $member[0]->Id;
        $this->apimodel->get("member_activity")->create($post);
        $this->Output(200,"sukses",[]);
    }

    public function member_connect()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $res = $this->apimodel->query("SELECT Created FROM member_activity WHERE MemberId = ".$member[0]->Id." AND DATE(Created) = '".date("Y-m-d")."' ORDER BY Created DESC LIMIT 1");
        if(count($res) > 0){

            $last = strtotime($res[0]->Created);
            $now = strtotime(date("Y-m-d H:i:s"));
            $selisih = ($now - $last) / 3600;

            $post = [
                "Steps"=>0,
                "Hours"=>0,
                "Distance"=>0,
                "LoginCount"=>0,
                "ConnectHours"=>$selisih
            ];
            $post["MemberId"] = $member[0]->Id;
            $this->apimodel->get("member_activity")->create($post);
            $this->Output(200,"success",[]);
        } else {
            $post = [
                "Steps"=>0,
                "Hours"=>0,
                "Distance"=>0,
                "LoginCount"=>0,
                "ConnectHours"=>0
            ];
            $post["MemberId"] = $member[0]->Id;
            $this->apimodel->get("member_activity")->create($post);
            $this->Output(200,"success",[]);
        }
        
    }

    public function mission_check()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $mission = $this->input->post("mission_id");
        $check = $this->apimodel->get("missions")->where(["Id"=>$mission]);
        $activity = $this->apimodel->query("SELECT SUM(Steps) TotalStep, SUM(Distance) TotalDist, SUM(Hours) TotalHour, SUM(LoginCount) TotalLogin, SUM(ConnectHours) TotalConnect FROM member_activity WHERE MemberId = ".$member[0]->Id." AND Created >= '".$check[0]->Created."' AND Created <= '".$check[0]->Expired."' GROUP BY MemberId");
        $data = [];
        $mission_type = ["","Daily Mission","Weekly Mission","Monthly Mission","Yearly Mission"];
        $data["MissionType"] = $mission_type[$check[0]->Type];
        if(count($activity) > 0){
        switch($check[0]->Title){
            case "Weekly Login":
            case "Monthly Login":
            case "Daily Login": 
                $data["MissionTitle"] = $check[0]->Title;
                $percentage = ($activity[0]->TotalLogin / $check[0]->LoginCount);
                $data["YourMissionProgress"] = $percentage >= 1 ? "100%" : round((100 * $percentage),1)."%";
                $data["MaxMissionProgress"] = "100%";
                if($percentage >= 1){
                    //mission complete
                }
                break;
            case "Weekly Connected":
            case "Monthly Connected": break;
            case "Step And Distance": 
                $data["MissionTitle"] = $check[0]->Title;
                $step = ($activity[0]->TotalStep / $check[0]->Steps);
                $dist = ($activity[0]->TotalDist / $check[0]->Distance);
                $data["YourMissionStepProgress"] = $step >= 1 ? "100%" : round((100 * $step),1)."%";
                $data["MaxMissionStepProgress"] = "100%";
                $data["YourMissionDistanceProgress"] = $dist >= 1 ? "100%" : round((100 * $dist),1)."%";
                $data["MaxMissionDistanceProgress"] = "100%";
                if($dist >= 1 && $step >= 1){
                    //mission complete
                    $posted["MissionId"] = $mission;
                    $posted["MemberId"] = $member[0]->Id;
                    $posted["Exp"] = $check[0]->Exp;
                    $posted["AP"] = $check[0]->AP;
                    $posted["Status"] = 1;

                    $chk = $this->apimodel->get('member_mission')->where(['MemberId'=>$member[0]->Id,'MissionId'=>$mission]);

                    if(count($chk) == 0){
                        $this->apimodel->get('member_mission')->create($posted);
                    }
                }
                break;
            case "Hour And Distance": 
                $data["MissionTitle"] = $check[0]->Title;
                $hour = ($activity[0]->TotalHour / $check[0]->Hours);
                $dist = ($activity[0]->TotalDist / $check[0]->Distance);
                $data["YourMissionHourProgress"] = $hour >= 1 ? "100%" : round((100 * $hour),1)."%";
                $data["MaxMissionHourProgress"] = "100%";
                $data["YourMissionDistanceProgress"] = $dist >= 1 ? "100%" : round((100 * $dist),1)."%";
                $data["MaxMissionDistanceProgress"] = "100%";
                if($dist >= 1 && $hour >= 1){
                    //mission complete
                }
                break;
        }
        }
       
        $this->Output(200,"sukses",$data);  
    }

    public function mission_list()
    {
        $mission = $this->input->get("mission_type") == "" ? "" : "Type = ".$this->input->get("mission_type")." AND";
        $res = $this->apimodel->query("SELECT * FROM missions WHERE $mission DATE(Expired) >='".date("Y-m-d")."'");
        foreach($res as $row){
            switch($row->Type){
                case 1: $row->MissionType = "Daily"; break;
                case 2: $row->MissionType = "Weekly"; break;
                case 3: $row->MissionType = "Monthly"; break;
                case 4: $row->MissionType = "Yearly"; break;
            }
        }
        $this->Output(200,"sukses",$res); 
    }

    public function find_friend()
    {
        $token = $this->getHeader("x-token");
        // $this->member_connect($token);
         $member = $this->getMember($token);
        $item = $this->input->get("term");
        $term = $item == "" ? "" : "WHERE a.Fullname LIKE '%".$item."%'";
        $res = $this->apimodel->query("SELECT a.Id,a.Fullname,b.MemberLevel FROM members a JOIN member_profile b ON a.Id = b.MemberId $term");
        $result = []; 
        foreach($res as $row){
           
            if($row->Id != $member[0]->Id){
                $ret = $this->apimodel->query("SELECT Id FROM friend_list WHERE (FriendMemberId = ".$row->Id ." AND MemberId = ".$member[0]->Id.") OR (MemberId = ".$row->Id ." AND FriendMemberId = ".$member[0]->Id.") AND Status = 1");
                $rest["IsFriend"] = count($ret) > 0 ? true : false; 
                $rest["Id"] = $row->Id;
                $rest["Fullname"] = $row->Fullname;
                $rest["MemberLevel"] = $row->MemberLevel;
                array_push($result,$rest);
            }
 
        }
        $this->Output(200,"sukses",$result);
    }

    public function request_friend()
    {
        $token = $this->getHeader("x-token");
       // $this->member_connect($token);
        $member = $this->getMember($token);
        $friend = $this->input->post("requested_member_id");
        $unique = rand(1000,9000);
        $post = ["UniqueId"=>$unique,"MemberId"=>$member[0]->Id,"FriendMemberId"=>$friend,"Status"=>0];
        $this->apimodel->get("friend_list")->create($post);
        $this->Output(200,"sukses",[]);
    }

    public function request_list()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $res = $this->apimodel->query("SELECT a.Id,a.Fullname,b.MemberLevel,c.UniqueId FROM friend_list c JOIN members a ON a.Id = c.FriendMemberId JOIN member_profile b ON a.Id = b.MemberId WHERE c.Status = 0"); 
        $this->Output(200,"sukses",$res);
    }

    public function request_confirm()
    {
        $unique = $this->input->get("unique_id");
        $res = $this->apimodel->query("SELECT Id FROM friend_list WHERE UniqueId = $unique"); 
        if(count($res) > 0){
            $this->apimodel->get("friend_list")->row($res[0]->Id)->update(["Status"=>1]);
            $this->Output(200,"sukses",[]);
        } else {
            $this->Output(403,"whoops, invalid friend request",[]);
        }
       
    }

    public function request_unfriend()
    {
        $unique = $this->input->get("unique_id");
        $res = $this->apimodel->query("SELECT Id FROM friend_list WHERE UniqueId = $unique"); 
        if(count($res) > 0){
            $this->apimodel->get("friend_list")->row($res[0]->Id)->update(["Status"=>2]);
            $this->Output(200,"sukses",[]);
        } else {
            $this->Output(403,"whoops, invalid friend request",[]);
        }
       
    }

    public function friend_list()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
         $res = $this->apimodel->query("SELECT c.* FROM friend_list c WHERE c.Status = 1 AND (c.MemberId = ".$member[0]->Id." OR c.FriendMemberId = ".$member[0]->Id.")"); 
         $data = [];
         foreach($res as $row){
             $rows = [];
             $rows["UniqueId"] = $row->UniqueId;
             if($row->MemberId == $member[0]->Id){
                 $fr2 = $this->apimodel->query("SELECT * FROM members a JOIN member_profile b ON a.Id = b.MemberId WHERE a.Id = ".$row->FriendMemberId);
                 $fol = $this->apimodel->query("SELECT COUNT(MemberId) foll FROM friend_follow WHERE FollowedMemberId = ".$row->FriendMemberId);
                 $rows["FriendFollowerCount"] = $fol[0]->foll;
                 $rows["FriendLevel"] = $fr2[0]->MemberLevel;
                 $rows["FriendFullname"] = $fr2[0]->Fullname;
                 $rows["FriendPhoto"] = base_url()."theme/uploads/".$fr2[0]->Photo;
             } else if($row->FriendMemberId == $member[0]->Id){
                $fr = $this->apimodel->query("SELECT * FROM members a JOIN member_profile b ON a.Id = b.MemberId WHERE a.Id = ".$row->MemberId);
                $fol = $this->apimodel->query("SELECT COUNT(MemberId) foll FROM friend_follow WHERE FollowedMemberId = ".$row->MemberId);
                $rows["FriendFollowerCount"] = $fol[0]->foll;
                $rows["FriendLevel"] = $fr[0]->MemberLevel;
                $rows["FriendFullname"] = $fr[0]->Fullname;
                $rows["FriendPhoto"] = base_url()."theme/uploads/".$fr[0]->Photo;
             }
             array_push($data,$rows);
         }
        $this->Output(200,"sukses",$data);
    }

    public function follow_friend()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $foll = $this->input->post("followed_member_id");
        $this->apimodel->get("friend_follow")->create(["MemberId"=>$member[0]->Id,"FollowedMemberId"=>$foll]);
        $this->Output(200,"sukses",[]);
    }

    public function bug_report()
    {
        $post = $this->input->post();
        if(isset($_FILES["BugFile"])){
            $post["BugFile"] = $this->doUpload("BugFile");
        }
        $this->apimodel->get("bug_report")->create($post);
        $this->Output(200,"sukses",[]);
    }

    public function bank_account()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $post = $this->input->post();
        $post["UserId"] = $member[0]->Id;
        $this->apimodel->get("bank_account")->create($post);
        $this->Output(200,"sukses",[]);
    }

    public function get_contact()
    {
        $res = $this->apimodel->get("shue_profile")->where([]);
        $this->Output(200,"sukses",$res);
    }

    public function send_contact()
    {
        $post = $this->input->post();
        $this->apimodel->get("shue_contact")->create($post);
        $this->Output(200,"sukses",[]);
    }


    public function achievement_check()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token);
        $title = $this->input->post("title");
        $return = [];
        $check = $this->apimodel->query("SELECT SUM(Steps) ach_s, SUM(Distance) ach_d, SUM(Hours) ach_h, SUM(LoginCount) ach_l, SUM(ConnectHours) ach_c FROM member_activity");
        switch($title){
            case "step": 
                //bronze
                $ach = $this->apimodel->get("achievements")->where(["Level"=>"Bronze"]); 
                if(count($ach) > 0 && $ach[0]->Steps == $check[0]->ach_s && $ach[0]->Distance == $check[0]->ach_d){
                   $return["Bronze"] = true;
                    //insert ke member achievement
                } else {
                    $return["Bronze"] = false;
                }

                //silver
                $ach = $this->apimodel->get("achievements")->where(["Level"=>"silver"]);
                if(count($ach) > 0 && $ach[0]->Steps == $check[0]->ach_s && $ach[0]->Distance == $check[0]->ach_d){
                        $return["Silver"] = true;
                        //insert ke member achievement
                } else {
                        $return["Silver"] = false;
                }

                break;
        }
      $this->Output(200,"success",$return);
    }

    public function article_list()
    {
        $find = $this->input->get("find");
        $limit = $this->input->get("limit");
        $this->db->select("Id,Created,Title,Excerpt,BannerImage");
        $this->db->from("article");

        if($find != ""){
            $this->db->like('Title',$find);
            $this->db->or_like('Excerpt',$find);
        }

        if($limit != ""){
           $this->db->limit($limit);
        } else {
            $this->db->limit(25);
        }
        $res = $this->db->get()->result();
        foreach($res as $row){
            $row->BannerImage = base_url()."uploads/".$row->BannerImage;
        }
        $this->Output(200,"success",$res);
               
    }

    public function article_detail()
    {
        $id = $this->input->get("id");
        $this->db->select("*");
        $this->db->from("article"); 
        $this->db->where(["Id"=>$id]);
        $res = $this->db->get()->result(); 
        foreach($res as $row){
            $row->BannerImage = base_url()."uploads/".$row->BannerImage;
        }
        $this->Output(200,"success",$res);
               
    }

    public function member_profile()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token); 
        $row = $member[0];
        /* where yg blm di-redeem */
        $gift = $this->apimodel->query("SELECT * FROM gift WHERE Id NOT IN (SELECT GiftId FROM gift_redeem WHERE UserId = $row->Id) ORDER BY Id DESC LIMIT 1");
        $res["Email"] = $row->Email;
        $res["Fullname"] = $row->Fullname;
        $res["Phone"] = $row->Phone;
        $res["Photo"] = is_null($row->Photo) ? "" : base_url()."uploads/".$row->Photo;
        $res["ReferralCode"] = $row->RefCode;
        $res["Gift"] = count($gift) > 0 ? $gift[0] : null;
        $res["IsVerified"] = is_null($row->Ektp) ? "0" : "1";
        $res["IdCardData"] = ["EktpId"=>$row->EktpId,"EktpImage"=>base_url()."uploads/".$row->Ektp];
        $this->Output(200,"success",$res);
    }

    public function gift_redeem()
    {
         $token = $this->getHeader("x-token");
         $member = $this->getMember($token); 
         $row = $member[0];
         $gift = $this->input->get("gift_id");
         $this->apimodel->get("gift_redeem")->create([
             "GiftId"=>$gift,
             "UserId"=>$row->Id,
             "RedeemDate"=>date("Y-m-d H:i:s")
         ]);
         $this->Output(200,"success",[]);
    }

    public function gift_redeem_hist()
    {
        $token = $this->getHeader("x-token");
        $member = $this->getMember($token); 
        $row = $member[0];
        $res = $this->apimodel->get("gift_redeem")->where(["UserId"=>$row->Id]);
        foreach($res as $row){
            $row->RedeemStatus = ($row->RedeemStatus == "0")? "Diproses" : "Selesai";
        }
        $this->Output(200,"success",$res);
    }

    public function avail_gift()
    {
        $res = $this->apimodel->get("gift")->where([]);
        $this->Output(200,'success',$res);
    }

    public function socmed_list()
    {
        $res = $this->apimodel->get('socmed')->where([]);
        $this->Output(200,'success',$res);
    }

    //////////////////////////////////////////////////////////////

    public function cron_mission()
    {
        $cur_day = date("N");
        $cur_date = date("j");
        $data = [];
        /* weekly mission */
        if($cur_day == 1){
            $posta = [
                "Steps"=>25000,
                "Hours"=>0,
                "Distance"=>25,
                "Exp"=>500,
                "LoginCount"=>0,
                "ConnectHours"=>0,
                "AP"=>2,
                "Type"=>2,
                "Expired"=>date("Y-m-d H:i:s",strtotime("+1 week")),
                "Title"=>"Step And Distance"
            ];
            
           $res = $this->apimodel->get("missions")->create($posta); 
           $posta = [
            "Steps"=>0,
            "Hours"=>25,
            "Distance"=>100,
            "Exp"=>500,
            "LoginCount"=>0,
            "ConnectHours"=>0,
            "AP"=>2,
            "Type"=>2,
            "Expired"=>date("Y-m-d H:i:s",strtotime("+1 week")),
            "Title"=>"Hour And Distance"
        ];
        
       $res = $this->apimodel->get("missions")->create($posta); 
       $posta = [
        "Steps"=>0,
        "Hours"=>0,
        "Distance"=>0,
        "LoginCount"=>7,
        "ConnectHours"=>0,
        "Exp"=>500,
        "AP"=>2,
        "Type"=>2,
        "Expired"=>date("Y-m-d H:i:s",strtotime("+1 week")),
        "Title"=>"Weekly Login"
            ];
    
        $res = $this->apimodel->get("missions")->create($posta); 
        $posta = [
            "Steps"=>0,
            "Hours"=>0,
            "Distance"=>0,
            "LoginCount"=>0,
            "ConnectHours"=>35,
            "Exp"=>500,
            "AP"=>2,
            "Type"=>2,
            "Expired"=>date("Y-m-d H:i:s",strtotime("+1 week")),
            "Title"=>"Weekly Connected"
                ];

        $res = $this->apimodel->get("missions")->create($posta); 
           $data["weekly"] = "weekly added";
        }
        /* monthly mission */
        if($cur_date == 1){
            $postb = [
                "Steps"=>100000,
                "Hours"=>0,
                "Distance"=>100,
                "Exp"=>2500,
                "LoginCount"=>0,
                "ConnectHours"=>0,
                "AP"=>10,
                "Type"=>3,
                "Expired"=>date("Y-m-d H:i:s",strtotime("+1 month")),
                "Title"=>"Step And Distance"
            ];
            $res = $this->apimodel->get("missions")->create($postb); 
             
                $postb = [
                    "Steps"=>0,
                    "Hours"=>100,
                    "Distance"=>400,
                    "Exp"=>2500,
                    "LoginCount"=>0,
                    "ConnectHours"=>0,
                    "AP"=>10,
                    "Type"=>3,
                    "Expired"=>date("Y-m-d H:i:s",strtotime("+1 month")),
                    "Title"=>"Hour And Distance"
                ];
                $res = $this->apimodel->get("missions")->create($postb);
                
                    $postb = [
                        "Steps"=>0,
                        "Hours"=>0,
                        "Distance"=>0,
                        "Exp"=>2500,
                        "LoginCount"=>31,
                        "ConnectHours"=>0,
                        "AP"=>10,
                        "Type"=>3,
                        "Expired"=>date("Y-m-d H:i:s",strtotime("+1 month")),
                        "Title"=>"Monthly Login"
                    ];
                    $res = $this->apimodel->get("missions")->create($postb);
                    $postb = [
                        "Steps"=>0,
                        "Hours"=>0,
                        "Distance"=>0,
                        "Exp"=>2500,
                        "LoginCount"=>0,
                        "ConnectHours"=>120,
                        "AP"=>10,
                        "Type"=>3,
                        "Expired"=>date("Y-m-d H:i:s",strtotime("+1 month")),
                        "Title"=>"Monthly Connected"
                    ];
                    $res = $this->apimodel->get("missions")->create($postb);
            $data["monthly"] = "monthly added";
        } 
        /* daily mission */
            $postc = [
                "Steps"=>0,
                "Hours"=>0,
                "Distance"=>0,
                "Exp"=>20,
                "LoginCount"=>1,
                "ConnectHours"=>0,
                "AP"=>1,
                "Type"=>1,
                "Expired"=>date("Y-m-d H:i:s",strtotime("+1 day")),
                "Title"=>"Daily Login"
            ];
            $data["daily"] = "daily added";
            $res = $this->apimodel->get("missions")->create($postc); 
        
        $this->Output(200,"sukses",$data);
    }

    public function admin_about_save()
    {
        $id = $this->input->get("id");
        $this->apimodel->get("about")->row($id)->update(["Content"=>$this->input->post("Content")]);
    }

    public function admin_about_load()
    {
        $id = $this->input->get("id");
        $res = $this->apimodel->get("about")->where([]);
        $this->Output(200,"success",$res);
    }

    public function admin_mission_add()
    {
        $post = $this->input->post();
        switch($post["Type"]){
            case "1": //expired 1 day
                $post["Expired"] = date("Y-m-d H:i:s",strtotime("+1 day"));
                break;
            case "2": //expired 1 week
                $post["Expired"] = date("Y-m-d H:i:s",strtotime("+1 week"));
                break;
            case "3": //expired 1 month
                $post["Expired"] = date("Y-m-d H:i:s",strtotime("+1 month"));
                break;
            case "4": //expired 1 year
                $post["Expired"] = date("Y-m-d H:i:s",strtotime("+1 year"));
                break;
        }
        $res = $this->apimodel->get("missions")->create($post); 
        $this->Output(200,"sukses",[]);
    }
    public function admin_mission_update()
    {
        $id = $this->input->get("id");
        $post = $this->input->post();
        $mission = $this->apimodel->get("missions")->where(["Id"=>$id]);
        $created = date_create($mission[0]->Created);
        switch($post["Type"]){
            case "1": //expired 1 day
                $interval = date_add($created,date_interval_create_from_date_string("1 days"));
                $expired = date_format($created,"Y-m-d H:i:s");
                $post["Expired"] = $expired;
                break;
            case "2": //expired 1 week
                $interval = date_add($created,date_interval_create_from_date_string("1 weeks"));
                $expired = date_format($created,"Y-m-d H:i:s");
                $post["Expired"] = $expired;
                break;
            case "3": //expired 1 month
                $interval = date_add($created,date_interval_create_from_date_string("1 months"));
                $expired = date_format($created,"Y-m-d H:i:s");
                $post["Expired"] = $expired;
                break;
            case "4": //expired 1 year
                $interval = date_add($created,date_interval_create_from_date_string("1 years"));
                $expired = date_format($created,"Y-m-d H:i:s");
                $post["Expired"] = $expired;
                break;
        }
         $res = $this->apimodel->get("missions")->row($id)->update($post); 
        $this->Output(200,"sukses",[]);
    }
    public function admin_achievement_add()
    {
        $post = $this->input->post();
        $this->apimodel->get("achievements")->create($post);
        $this->Output(200,"sukses",[]);
    }
    public function admin_achievement_update()
    {
        $id = $this->input->get("id");
        $post = $this->input->post();
        $res = $this->apimodel->get("achievements")->row($id)->update($post); 
        $this->Output(200,"sukses",[]);

    }
    public function gift_redeem_list()
    {
        $res = $this->apimodel->query("SELECT a.RedeemStatus,a.Id,c.Fullname,b.Name FROM gift b JOIN gift_redeem a ON a.GiftId = b.Id LEFT JOIN members c ON a.UserId = c.Id");
        $this->Output(200,"success",$res); 
    }
    public function admin_gift_redeem_update()
    {
        $id = $this->input->get("id"); 
        $post = $this->input->post(); 
        $this->apimodel->get("gift_redeem")->row($id)->update($post);
        $this->Output(200,"success",[]);
    }
    public function gift_list()
    {
        $res = $this->apimodel->get("gift")->where([]);
        $this->Output(200,"success",$res); 
    }
    public function admin_gift_add()
    {
            $post = $this->input->post(); 
            $this->apimodel->get("gift")->create($post);
            $this->Output(200,"success",[]); 
    }
    public function admin_gift_edit()
    {
        $id = $this->input->get("id");
        $res = $this->apimodel->get("gift")->where(["Id"=>$id]);
        $this->Output(200,"success",$res);
    }
    public function admin_gift_update()
    {
            $id = $this->input->get("id"); 
            $post = $this->input->post(); 
            $this->apimodel->get("gift")->row($id)->update($post);
            $this->Output(200,"success",[]);
        
    }
    public function admin_gift_delete()
    {
        $id = $this->input->get("id"); 
        $this->apimodel->get("gift")->row($id)->delete();
        $this->Output(200,"success",[]);
    }
    public function admin_article_add()
    {
        if(isset($_FILES["BannerImage"])){
            $post = $this->input->post();
            $post["BannerImage"] = $this->doUpload("BannerImage");
            $post["Excerpt"] = substr(strip_tags($this->input->post("Content")),0,25)."...";
            $this->apimodel->get("article")->create($post);
            $this->Output(200,"success",[]);
        } else {
            $this->Output(403,"oops, banner image required",[]);
        }
    }
    public function admin_article_update()
    {
        $id = $this->input->get("id");
        if(isset($_FILES["BannerImage"])){
            $post["Title"] = $this->input->post("Title");
            $post["Content"] = $this->input->post("Content");
            $res = $this->apimodel->get("article")->where(["Id"=>$id]);
            if(file_exists($this->path."/uploads/".$res[0]->BannerImage)){
                unlink($this->path."/uploads/".$res[0]->BannerImage);
            }
            $post["Excerpt"] = substr(strip_tags($this->input->post("Content")),0,25)."...";
            $post["BannerImage"] = $this->doUpload("BannerImage");
            $this->apimodel->get("article")->row($id)->update($post);
            $this->Output(200,"success",[]);
        } else {
            $post["Title"] = $this->input->post("Title");
            $post["Content"] = $this->input->post("Content"); 
            $post["Excerpt"] = substr(strip_tags($this->input->post("Content")),0,25)."...";
            $this->apimodel->get("article")->row($id)->update($post);
            $this->Output(200,"success",[]);
        }
    }
    public function admin_article_edit()
    {
        $id = $this->input->get("id");
        $res = $this->apimodel->get("article")->where(["Id"=>$id]);
        $this->Output(200,"success",$res);
    }
    public function admin_article_delete()
    {
        $id = $this->input->get("id");
        $res = $this->apimodel->get("article")->where(["Id"=>$id]);
        if(file_exists($this->path."/uploads/".$res[0]->BannerImage)){
            unlink($this->path."/uploads/".$res[0]->BannerImage);
        }
        $this->apimodel->get("article")->row($id)->delete();
        $this->Output(200,"success",[]);
    }
    public function admin_login_check()
    {
        $data["Username"] = $this->input->post("Username");
        $data["Password"] = md5($this->input->post("Password"));
        $res = $this->apimodel->get("users")->where($data);
        if(count($res) > 0){
            $this->Output(200,"success",$res);
        } else {
            $this->Output(412,"oops, invalid user",$res);
        }

    }
    public function admin_module()
    { 
        $res = $this->apimodel->get("modules")->where([]);
        if(count($res) > 0){
            $this->Output(200,"success",$res);
        } else {
            $this->Output(412,"oops, invalid user",$res);
        }

    }
    public function admin_socmed_load()
    {
        $res = $this->apimodel->get('socmed')->where([]);
        $this->Output(200,'success',$res);
    }
    public function admin_socmed_save()
    {
        $post = $this->input->post();
        $id = $this->input->get("id");
        $this->apimodel->get('socmed')->row($id)->update($post);
    }


    }