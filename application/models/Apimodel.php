<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");

class Apimodel extends CI_Model {
   private $table;
   private $primary;
   public function __construct()
   {
       parent::__construct();
   }
   public function get($table)
   {
       $this->table = $table;
       return $this;
   }
   public function row($id)
   {
       $this->primary = $id;
       return $this;
   }
   public function create($data)
   {
       $this->db->insert($this->table,$data);
       return $this->db->insert_id();
   }
   public function update($data)
   {
       $this->db->where("Id",$this->primary);
       $this->db->update($this->table,$data);
   }
   public function where($where)
   {
       $res = $this->db->select("*")
              ->from($this->table)
              ->where($where)
              ->get()
              ->result();
      return $res;
   }
   public function delete()
   {
    $this->db->where("Id",$this->primary);
    $this->db->delete($this->table);
   }
   private function param_resolver($param)
   {
       /* struktur param
       [
           "where" => [
               "key"=>"value",
               "key <="=>"value"
           ],
           "like" => [
               "key"=>"value"
           ],
            "where_or" => [
               "key"=>"value",
               "key <="=>"value"
           ],
           "like_or" => [
               "key"=>"value"
           ],
           "join" => [
               "table"=>"on definition"
           ],
           "left_join" => [
               "table"=>"on definition"
           ],
           "order" => [
               "kolom"=>"desc"
           ],
       ]
       */
      $where = "";
      foreach($param as $key => $val){
           if($key == "where"){
               foreach($val as $k => $v){
                  $stat =  preg_match("/[<>=]/",$k);
                  $kx = $stat == 1 ? $k : $k."=";
                   $where .= " $kx '$v' AND ";
               }
           }
      }
   }
   public function first($param)
   {

   }
   public function query($sql)
   {
       $res = $this->db->query($sql)->result();
       return $res;
   }
}

?>